package pr03;

import lib.TextIO;

public class CumLaude {
	public static void main(String[] args) {

		/*
		 * Kasutaja peab sisestama keskmise hinde kui ka l�put�� hinde.
		 * Korrektne1 ja korrektne2 defineerivad hinde korrektse sisestamise
		 * meetodi. Valede sisestuste korral k�sitakse sisendeid uuesti.
		 * Sisesndite uuesti k�simise tagab do-while ts�kkel
		 */

		// muutujate defineerimine
		double keskhinne;
		int lopphinne;
		boolean korrektne1;
		boolean korrektne2;

		// do-while ts�kkel
		do {
			// keskmise hinde ja l�put�� hinde sisestamine
			System.out.print("Sisesta oma keskmine hinne: ");
			keskhinne = TextIO.getlnDouble();
			System.out.print("Sistest oma l�put�� hinne: ");
			lopphinne = TextIO.getlnInt();

			// Hinnete korrektse sisestamise kriteeriumid
			korrektne1 = keskhinne > 1 && keskhinne <= 5;
			korrektne2 = lopphinne > 1 && lopphinne <= 5;

			// Keskmine hinne valesti sisestamise korral ...
			if (!korrektne1 && korrektne2) {
				System.out.println("Keskmine hinne peab olema positiivne number vahemikus 1 kuni 5.");
			}

			// L�put�� hinde valesti sisestamise korral ...
			else if (korrektne1 && !korrektne2) {
				System.out.print("L�put�� hinne peab olema positiivne number vahemikus 1 kuni 5.");
			}

			// M�lema valesti sisestamise korral ...
			else if (!korrektne1 && !korrektne2) {
				System.out.println("Keskmine hinne peab olema positiivne number vahemikus 1 kuni 5.");
				System.out.println("L�put�� hinne peab olema positiivne number vahemikus 1 kuni 5.");
			}

			// do-while ts�kkel katkeb, kui hinded on korrektselt sisestatud
			// teiste s�nadega: do-while ts�kkel kestab, kuni sisestatakse
			// ebakorrekteid andmeid
		} while (!korrektne1 || !korrektne2);

		// Cum Laude v�ljakuulutamine
		if (keskhinne >= 4.5 && lopphinne == 5) {
			System.out.print("Jah, saad cum laude diplomi!");
		} else {
			System.out.print("Ei saa!");
		}

	}

}
