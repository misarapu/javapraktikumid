package pr06;

public class MyndiViskamine {

	public static void main(String[] args) {

		// muutujate defineerimine
		int pakkumine;
		int raha = 100; // algsaldo
		int panus;

		// While-do ts�kkel kestab, kuni raha veel on ( raha > 0 )
		do {

			// m�ndiviskamine (v�ib kasutada ka suvalise arvu genereerimise
			// meetodit)
			int mynt = (int) (Math.random() * 2);
			// System.out.println(mynt);

			// panuse k�simine, kontroll, et m�ngija ei paneks m�ngu rohkem kui
			// 25 v�i ta panus ei oleks suurem, kui tema j�relej��nud punktide
			// arv
			System.out.println("Sisesta panus (max 25 punkti): ");
			panus = TextIO.getlnInt();
			while (panus > 25 || panus > raha) {
				if (panus > 25) {
					System.out.println("Liiga suur panus. Sisesta v�iksem panus.");
				} else if (panus > raha && raha < 25) {
					System.out.println("Sul ei ole nii palju raha. Sisesta v�iksem panus.");
				}
				panus = TextIO.getlnInt();
			}

			// kulli v�i kirja �ra arvamine
			System.out.println("Kas tuleb kull (0) v�i kiri (1)?");
			pakkumine = TextIO.getlnInt();

			// �ige pakkumine ja punktide juurde liitmine
			if (pakkumine == mynt) {
				raha += panus;
				System.out.println("Arvasid �igesti ja sul on n��d " + raha + " punkti.");
			}

			// vale pakkumine ja punktide v�hemaks v�tmine
			else if (pakkumine != mynt) {
				raha -= panus;
				System.out.println("Arvasid valesti ja sul on n��d " + raha + " punkti.");
			}

		} while (raha > 0);

		// siis kui raha otsa saab
		if (raha <= 0) {
			System.out.println("Punktid said otsa. M�ng on l�bi");
		}
	}
}
